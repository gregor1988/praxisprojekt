#pragma once

#include <QWidget>
#include <QGridLayout>
#include <QRadioButton>
#include <QGroupBox>
#include <QLabel>
#include <QComboBox>
#include <QStandardItemModel>
#include <QDataWidgetMapper>
#include <QSpinBox>
#include <QDebug>
#include <QStringListModel>
#include "MetaDataContainer.h"

using namespace Qt;

class RecOptions : public QWidget
{
	Q_OBJECT

public:
	RecOptions();
	~RecOptions();
	//getters
	MetaDataContainer *getMetaData();
	QSpinBox* RecOptions::getFpsSpinner();

private:
	MetaDataContainer *metaData;

	QRadioButton *samplingbtn;
	QRadioButton *recordbtn;
	QGroupBox *radioGroup;
	
	QLabel *frameLabel;
	QSpinBox *frameSpinner;
	QComboBox *frameCombo;

	QLabel *stopLabel;
	QSpinBox *stopSpinner;
	QComboBox *stopCombo;

	QLabel *fpsLabel;
	QSpinBox *fpsSpinner;

	QGridLayout *grid;

	QStringList types;

	QStringListModel *typeModel;
	
signals:
	void emitMetaData(MetaDataContainer*);

public slots:
	void sendMetaData();
	void disableFpsSpinner(bool);

};
