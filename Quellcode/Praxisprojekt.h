#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_Praxisprojekt.h"

class Praxisprojekt : public QMainWindow
{
	Q_OBJECT

public:
	Praxisprojekt(QWidget *parent = Q_NULLPTR);

private:
	Ui::PraxisprojektClass ui;
};
