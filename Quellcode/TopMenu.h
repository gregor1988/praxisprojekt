#pragma once

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QRadiobutton>
#include <QGroupBox>
#include <QString>
#include <QLabel>
#include <QFileDialog>
#include <QtCore>
#include <QMainWindow>
#include <QString>
#include <QStateMachine>

class TopMenu : public QWidget
{
	Q_OBJECT

public:
	TopMenu();
	QHBoxLayout *topMenuBar;
	~TopMenu();
	double scaleFactor;

	QString getSavePath();
	QPushButton* getDirChooserBtn();
	QPushButton* getSaveBtn();
	QPushButton* getStartButton();

	bool getEmbedbtnState();
	bool getStartbtnState();

	QState* getStopState();
	QState* getStartState();

private:

	QPushButton *startButton;
	QPushButton *inZoomButton;
	QPushButton *outZoomButton;
	QPushButton *embedButton;

	QStateMachine Embedbtnswitch;
	QState *on;
	QState *off;

	QStateMachine startStopToggle;
	QState *startState;
	QState *stopState;

	QFileDialog *dialog;
	QString savePath;
	QPushButton *dirChooserButton;
	QPushButton *saveButton;

	QLabel *pathLabel;

	QHBoxLayout* chooseBox;
	QHBoxLayout* saveBox;
	QHBoxLayout* zoomBox;

	QWidget * spacer1;
	QWidget * spacer2;

	void debounce();

private slots:
	void zoomIn();
	void zoomOut();
	void openDialog();
};

