#include "ExposureTime.h"

ExposureTime::ExposureTime()
	: QWidget()
{
	//Widgets
	slider = new QSlider(Qt::Horizontal);
	slider->setRange(35, 9999990);
	slider->setTracking(false);
	slider->setEnabled(false);
	spinner = new QSpinBox();
	spinner->setAlignment(Qt::AlignRight);
	spinner->setRange(35, 9999990);
	QString micro = QString(QChar(181));
	QString second = "s";
	micro.append(second);
	spinner->setSuffix(micro);
	spinner->setSingleStep(35);
	spinner->setEnabled(false);

	//Label title
	titleLabel = new QLabel("Exposure Time");
	//autoExposure
	autoExposureLabel = new QLabel("Auto Exposure");
	checkBox = new QCheckBox(this);
	checkBox->setChecked(true);
	autoBox = new QHBoxLayout();
	autoBox->addWidget(autoExposureLabel);
	autoBox->addWidget(checkBox);

	//Sync Widgets
	QObject::connect(slider, SIGNAL(valueChanged(int)), this, SLOT(adaptSliderValue(int)));
	QObject::connect(this, SIGNAL(adaptedSliderValue(int)), spinner, SLOT(setValue(int)));
	QObject::connect(spinner, SIGNAL(valueChanged(int)), slider, SLOT(setValue(int)));

	QObject::connect(checkBox, SIGNAL(clicked()), SLOT(disableSettingValueBox()));
	
	
	//Default
	//Horizontal Box
	hBox = new QHBoxLayout();
	hBox->addWidget(slider);
	hBox->addWidget(spinner);

	//Vertical Box
	settingValuevBox = new QVBoxLayout();
	settingValuevBox->addWidget(titleLabel);
	settingValuevBox->addLayout(hBox);
	settingValuevBox->addLayout(autoBox);
	settingValuevBox->setAlignment(Qt::AlignLeft);
	settingValuevBox->setEnabled(true);


	this->setLayout(settingValuevBox);
	this->setMaximumHeight(120);
	this->setMaximumHeight(120);
}
QSpinBox* ExposureTime::getSpinner() { return spinner; }
QCheckBox* ExposureTime::getCheckBox() { return checkBox; }

void ExposureTime::adaptSliderValue(int n) {
	if (n % 35 == 0) {
		emit adaptedSliderValue(n);
	}
	else {
		if (n - 35 < 35) { 
			emit adaptedSliderValue(spinner->minimum()); 
		}
		else if(n + 35 > spinner->maximum()) {
			emit adaptedSliderValue(spinner->maximum()); 
		}else
		{ 
			int a = n;
			int b = n % 35;
			int x = n - (b);
			emit adaptedSliderValue(x);
		}
	}
}

void ExposureTime::disableSettingValueBox() {
	slider->setEnabled(!slider->isEnabled());
	spinner->setEnabled(!spinner->isEnabled());
}

ExposureTime::~ExposureTime()
{
}
