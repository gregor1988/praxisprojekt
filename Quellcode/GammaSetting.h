#pragma once

#include <QObject>
#include <QSlider>
#include <QDoubleSpinbox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>

class GammaSetting : public QWidget
{
	Q_OBJECT

public:
	GammaSetting();
	QSpinBox* getSpinner();
	~GammaSetting();
private:
	QSlider *slider;
	QSpinBox *spinner;
	QHBoxLayout *hBox;
	QLabel *label;
	QVBoxLayout *vBox;

};

