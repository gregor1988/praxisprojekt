#include "AOI.h"

AOI::AOI()
	: QWidget()
{
	//components + style
	title = new QLabel();
	title->setText("AOI");
	titleBox = new QHBoxLayout();
	titleBox->addWidget(title);

	offsetXSpinner = new QSpinBox();
	offsetXSlider = new QSlider(Qt::Horizontal);
	offsetXSlider->setTracking(false);
	offsetXSpinner->setPrefix("offset x: ");
	offsetXSpinner->setSuffix(" px");
	offsetXSpinner->setAlignment(Qt::AlignRight);
	offsetXBox = new QHBoxLayout();
	offsetXBox->addWidget(offsetXSlider);
	offsetXBox->addWidget(offsetXSpinner);
	offsetXSpinner->setRange(0,2526);
	offsetXSpinner->setSingleStep(2);
	offsetXSlider->setRange(0, 2526);
	offsetXSlider->setSingleStep(2);

	offsetYSpinner = new QSpinBox();
	offsetYSlider = new QSlider(Qt::Horizontal);
	offsetYSlider->setTracking(false);
	offsetYSpinner->setPrefix("offset y: ");
	offsetYSpinner->setSuffix(" px");
	offsetYSpinner->setAlignment(Qt::AlignRight);
	offsetYBox = new QHBoxLayout();
	offsetYBox->addWidget(offsetYSlider);
	offsetYBox->addWidget(offsetYSpinner);
	offsetYSpinner->setRange(0, 1878);
	offsetYSpinner->setSingleStep(2);
	offsetYSlider->setRange(0, 1878);
	offsetYSlider->setSingleStep(2);

	widthSpinner = new QSpinBox();
	widthSlider = new QSlider(Qt::Horizontal);
	widthSlider->setTracking(false);
	widthSpinner->setPrefix("width: ");
	widthSpinner->setSuffix(" px");
	widthSpinner->setAlignment(Qt::AlignRight);
	widthSpinner->setValue(2590);
	widthBox = new QHBoxLayout();
	widthBox->addWidget(widthSlider);
	widthBox->addWidget(widthSpinner);
	widthSpinner->setRange(64, 2590);
	widthSpinner->setSingleStep(2);
	widthSlider->setRange(64, 2590);
	widthSlider->setSingleStep(2);
	widthSlider->setValue(2590);

	heightSpinner = new QSpinBox();
	heightSlider = new QSlider(Qt::Horizontal);
	heightSlider->setTracking(false);
	heightSpinner->setPrefix("height: ");
	heightSpinner->setSuffix(" px");
	heightSpinner->setAlignment(Qt::AlignRight);
	
	heightBox = new QHBoxLayout();
	heightBox->addWidget(heightSlider);
	heightBox->addWidget(heightSpinner);
	heightSpinner->setRange(64, 1942);
	heightSpinner->setSingleStep(2);
	heightSlider->setRange(64, 1942);
	heightSlider->setSingleStep(2);
	heightSlider->setValue(1942);
	
	defaultButton = new QPushButton("Reset AOI");

	//sync Widgets
	connect(offsetXSlider, SIGNAL(valueChanged(int)), this, SLOT(makeOffsetXEven(int)));
	connect(this, SIGNAL(offsetXisEven(int)), offsetXSpinner, SLOT(setValue(int)));
	connect(offsetXSpinner, SIGNAL(valueChanged(int)), offsetXSlider, SLOT(setValue(int)));

	connect(widthSpinner, SIGNAL(valueChanged(int)), this, SLOT(setOffsetXValueInvertedIfNecessary(int)));

	connect(offsetYSlider, SIGNAL(valueChanged(int)), this, SLOT(makeOffsetYEven(int)));
	connect(this, SIGNAL(offsetYisEven(int)), offsetYSpinner, SLOT(setValue(int)));
	connect(offsetYSpinner, SIGNAL(valueChanged(int)), offsetYSlider, SLOT(setValue(int)));

	connect(offsetXSpinner, SIGNAL(valueChanged(int)), this, SLOT(setWidthInvertedIfNecessary(int)));

	connect(widthSlider, SIGNAL(valueChanged(int)), this, SLOT(makeWidthEven(int)));
	connect(this, SIGNAL(widthisEven(int)), widthSpinner, SLOT(setValue(int)));
	connect(widthSpinner, SIGNAL(valueChanged(int)), widthSlider, SLOT(setValue(int)));

	connect(heightSpinner, SIGNAL(valueChanged(int)), this, SLOT(setOffsetYValueInvertedIfNecessary(int)));

	connect(heightSlider, SIGNAL(valueChanged(int)), this, SLOT(makeHeightEven(int)));
	connect(this, SIGNAL(heightisEven(int)), heightSpinner, SLOT(setValue(int)));
	connect(heightSpinner, SIGNAL(valueChanged(int)), heightSlider, SLOT(setValue(int)));

	connect(offsetYSpinner, SIGNAL(valueChanged(int)), this, SLOT(setHeightInvertedIfNecessary(int)));

	connect(defaultButton, SIGNAL(clicked()), this, SLOT(defaultAOI()));

	aoiMenu = new QVBoxLayout();

	aoiMenu->addLayout(titleBox);
	aoiMenu->addLayout(offsetXBox);
	aoiMenu->addLayout(offsetYBox);
	aoiMenu->addLayout(widthBox);
	aoiMenu->addLayout(heightBox);
	aoiMenu->addWidget(defaultButton);
	aoiMenu->setAlignment(Qt::AlignLeft);

	this->setLayout(aoiMenu);
	this->setMinimumHeight(100);

	heightSpinner->setValue(1942);
	widthSpinner->setValue(2590);
}

AOI::~AOI()
{
}
//getters
QSpinBox* AOI::getOffsetXSpinner() { return offsetXSpinner; }
QSpinBox* AOI::getOffsetYSpinner(){ return offsetYSpinner; }
QSpinBox* AOI::getWidthSpinner(){ return widthSpinner; }
QSpinBox* AOI::getHeightSpinner(){ return heightSpinner; }

void AOI::debounce(int ms) {
	QTime dieTime = QTime::currentTime().addMSecs(ms);
	while (QTime::currentTime() < dieTime)
		QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}


//slots
void AOI::setOffsetXValueInvertedIfNecessary(int val) {
	qDebug() << "entered setOffsetXValueInvertedIfNecessary";
	if (2590 - offsetXSpinner->value() < val) {
		offsetXSlider->setValue(offsetXSpinner->value()-(offsetXSpinner->value()+val-2590));
	}

}

void AOI::setOffsetYValueInvertedIfNecessary(int val){
	if (1942 - offsetYSpinner->value() < val) {
		offsetYSlider->setValue(offsetYSpinner->value() - (offsetYSpinner->value() + val - 1942));
	}
}

void AOI::setWidthInvertedIfNecessary(int val) {
	if (val + widthSpinner->value() > 2590) {
		widthSlider->setValue(widthSpinner->value()-(val + widthSpinner->value() - 2590));
	}
};

void AOI::setHeightInvertedIfNecessary(int val) {
	
	if (val + heightSpinner->value() > 1942) {
		heightSlider->setValue(heightSpinner->value() - (val + heightSpinner->value() - 1942));
	}
};

void AOI::makeOffsetXEven(int val) {
	if (val % 2 != 0 && val + 1<1942) { val++; }
	else if(val % 2 != 0) { val--; }
	emit offsetXisEven(val);
}
void AOI::makeOffsetYEven(int val){
	if (val % 2 != 0 && val +1<1942) { val++; }
	else if(val % 2 != 0) { val--; }
	emit offsetYisEven(val);
}
void AOI::makeWidthEven(int val){
	if (val % 2 != 0 && val + 1<1942) { val++; }
	else if (val % 2 != 0) { val--; }
	emit widthisEven(val);
}
void AOI::makeHeightEven(int val){
	if (val % 2 != 0 && val + 1<1942) { val++; }
	else if (val % 2 != 0) { val--; }
	emit heightisEven(val);
}

void AOI::defaultAOI()
{
	emit defaultBtnClicked();
}

