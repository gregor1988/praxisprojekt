#pragma once

#include <QObject>
#include <QSlider>
#include <QDoubleSpinbox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QCheckBox>
#include <QDebug>

class ExposureTime : public QWidget
{
	Q_OBJECT

public:
	ExposureTime();
	QSpinBox* getSpinner();
	QCheckBox* getCheckBox();
	~ExposureTime();
	
private:
	//setValue area
	QSlider *slider;
	QSpinBox *spinner;
	QHBoxLayout *hBox;
	QLabel *titleLabel;
	//auto exposure
	
	QLabel *autoExposureLabel;
	QCheckBox *checkBox;
	QHBoxLayout *autoBox;

	QVBoxLayout *settingValuevBox;

signals:
	void adaptedSliderValue(int);
	void setAutoExposure(bool);
public slots:
	void adaptSliderValue(int);
	void disableSettingValueBox();

};

