#include "MyTimer.h"



MyTimer::MyTimer()
	: QObject()
{

	timer = new QTimer(this);
	start = 0;
	

	
}
QTimer* MyTimer::getTimer() { return timer; }

void MyTimer::startTimer(int ms,int stopms) {
	timer->start(ms);
	increment = ms;
	stop = stopms;
}


int MyTimer::getStart() { return start; }
int MyTimer::getStop() { return stop; }
int MyTimer::getIncrement(){ return increment; }
void MyTimer::incStart(int n) { start = start + n; }
void MyTimer::reset() { start = 0; timer->stop(); }

MyTimer::~MyTimer()
{
}
