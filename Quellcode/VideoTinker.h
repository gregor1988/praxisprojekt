#pragma once

#include <QtCore>
#include <iostream>
#include <QLabel>
#include <QPixmap>
#include <QImage>
#include <QThread>
#include <QtCore>


//Include OpenCV api
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2\opencv.hpp>
#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat)
#include <opencv2/videoio.hpp>  // Video write


class VideoTinker : public QThread
{
	Q_OBJECT

public:
	enum Mode {START,SAMPLING,RECORDING,END};
	Mode modus;
	VideoTinker();
	~VideoTinker();
	void run();
	QString path;
	int fps;
	int width;
	int height;
	void endVideo();

private:
	int fcc;
	uchar* bits;
	int steps;
	QImage qFrame;
	bool write;
	bool close;
	void writeVideo(QString);
	void writeSampleSerie(QString);
	int sampleNumber;
public slots:
	void writeFrame(QImage);
};
