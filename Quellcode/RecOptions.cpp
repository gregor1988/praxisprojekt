#include "RecOptions.h"

RecOptions::RecOptions()
	: QWidget()
{
	samplingbtn = new QRadioButton("Sampling");
	recordbtn = new QRadioButton("Recording");
	recordbtn->setChecked(true);
	radioGroup = new QGroupBox();

	types << tr("ms") << tr("s") << tr("min") << tr("h");
	typeModel = new QStringListModel(types, this);


	frameLabel = new QLabel("Record a frame every");
	frameSpinner = new QSpinBox();
	frameSpinner->setAlignment(AlignRight);
	frameSpinner->setRange(1, 1000);
	frameSpinner->setValue(100);
	frameCombo = new QComboBox();
	frameCombo->setModel(typeModel);
	frameCombo->setFixedHeight(30);
	frameCombo->setFixedWidth(60);

	stopLabel = new QLabel("Stop sampling after");
	stopSpinner = new QSpinBox();
	stopSpinner->setRange(1, 1000);
	stopSpinner->setValue(10);
	stopSpinner->setAlignment(AlignRight);
	stopCombo = new QComboBox();
	stopCombo->setModel(typeModel);
	stopCombo->setCurrentIndex(1);
	stopCombo->setFixedHeight(30);
	stopCombo->setFixedWidth(60);


	fpsLabel = new QLabel("Set playback speed");
	fpsSpinner = new QSpinBox();
	fpsSpinner->setSuffix(" fps");
	fpsSpinner->setRange(20, 100);
	fpsSpinner->setValue(20);
	fpsSpinner->setAlignment(AlignRight);
	fpsSpinner->setMaximumWidth(20);
	fpsSpinner->setMaximumHeight(20);

	grid = new QGridLayout();
	grid->addWidget(samplingbtn, 0, 0, 1, 1);
	grid->addWidget(recordbtn, 0, 1, 1, 1);
	grid->addWidget(frameLabel, 1, 0, 1, 1);
	grid->addWidget(frameSpinner, 2, 0, 1, 1);
	grid->addWidget(frameCombo, 2, 1, 1, 1);
	grid->addWidget(stopLabel, 3, 0, 1, 1);
	grid->addWidget(stopSpinner, 4, 0, 1, 1);
	grid->addWidget(stopCombo, 4, 1, 1, 1);
	grid->addWidget(fpsLabel, 5, 0, 1, 1);
	grid->addWidget(fpsSpinner, 6, 0, 1, 1);
	grid->setAlignment(AlignLeft);

	this->setLayout(grid);


	//connection
	connect(samplingbtn, SIGNAL(toggled(bool)), this, SLOT(disableFpsSpinner(bool)));

}

MetaDataContainer* RecOptions::getMetaData() { return metaData; }
QSpinBox* RecOptions::getFpsSpinner() { return fpsSpinner; }

void RecOptions::sendMetaData() {

	metaData = new MetaDataContainer(frameSpinner->value(), stopSpinner->value(), fpsSpinner->value(), frameCombo->currentIndex(), stopCombo->currentIndex());
	emit emitMetaData(metaData);
}

void RecOptions::disableFpsSpinner(bool b) {
	 fpsSpinner->setEnabled(!b);
	 qDebug() << "asdf";
	
}

RecOptions::~RecOptions()
{
}
