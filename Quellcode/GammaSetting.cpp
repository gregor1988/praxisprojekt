#include "GammaSetting.h"



GammaSetting::GammaSetting()
	:QWidget()
{
	//Widgets
	slider = new QSlider(Qt::Horizontal);
	slider->setRange(10, 39);
	slider->setTracking(false);
	spinner = new QSpinBox();
	spinner->setAlignment(Qt::AlignRight);
	spinner->setRange(10, 39);
	spinner->setSuffix("/10"); 
	//Label name
	label = new QLabel("Gamma");
	//Sync Widgets
	QObject::connect(spinner, SIGNAL(valueChanged(int)), slider, SLOT(setValue(int)));
	QObject::connect(slider, SIGNAL(valueChanged(int)), spinner, SLOT(setValue(int)));
	//Default
	//Horizontal Box
	hBox = new QHBoxLayout();
	hBox->addWidget(slider);
	hBox->addWidget(spinner);

	//Vertical Box
	vBox = new QVBoxLayout();
	vBox->addWidget(label);
	vBox->addLayout(hBox);
	vBox->setAlignment(Qt::AlignLeft);
	
	this->setLayout(vBox);
	this->setMaximumHeight(80);
	this->setMaximumHeight(80);

}


QSpinBox* GammaSetting::getSpinner()
{
	return spinner;
}

GammaSetting::~GammaSetting()
{
}
