#pragma once

#include "GammaSetting.h"
#include <QVBoxLayout>
#include <QScrollarea>
#include <QLabel>
#include "AOI.h"
#include "RecOptions.h"
#include "ExposureTime.h"

using namespace Qt;

class SettingsMenu
{
public:
	SettingsMenu();
	QLabel* settingsLabel;
	QWidget *menubar;
	GammaSetting *getGammaSetting();
	AOI *getAOISetting();
	RecOptions *getRecOptions();
	ExposureTime *getExposureTimeOptions();
	void disableAllOptions();
	void enableAllOptions();

	~SettingsMenu();
private:
	RecOptions *recOptions;
	QVBoxLayout *settingsMenu;
	AOI *aoi;
	GammaSetting *gamma;
	ExposureTime *exposureTimeOptions;

};

