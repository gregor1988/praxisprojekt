#pragma once

#include <QtCore>
#include <QVBoxLayout>
#include <QSpinBox>
#include <QSlider>
#include <QHBoxLayout>
#include <QLabel>
#include <QMutex>
#include <QTime>
#include <QPushButton>

#include <QDebug>


class AOI : public QWidget
{
	Q_OBJECT

public:
	AOI();
	QSpinBox* getOffsetXSpinner();
	QSpinBox* getOffsetYSpinner();
	QSpinBox* getWidthSpinner();
	QSpinBox* getHeightSpinner();
	~AOI();
private:
	QLabel *title;
	QHBoxLayout *titleBox;

	QSpinBox *offsetXSpinner;
	QSlider *offsetXSlider;
	QHBoxLayout *offsetXBox;

	QSpinBox *offsetYSpinner;
	QSlider *offsetYSlider;
	QHBoxLayout *offsetYBox;

	QSpinBox *widthSpinner;
	QSlider *widthSlider;
	QHBoxLayout *widthBox;

	QSpinBox *heightSpinner;
	QSlider *heightSlider;
	QHBoxLayout *heightBox;

	QPushButton * defaultButton;

	QVBoxLayout *aoiMenu;

	void debounce(int);

signals:
	void offsetXisEven(int);
	void offsetYisEven(int);
	void widthisEven(int);
	void heightisEven(int);
	void defaultBtnClicked();

//Klasseninterne slots
private slots:
	void setOffsetXValueInvertedIfNecessary(int);
	void setWidthInvertedIfNecessary(int);
	void setOffsetYValueInvertedIfNecessary(int);
	void setHeightInvertedIfNecessary(int);

	void makeOffsetXEven(int);
	void makeOffsetYEven(int);
	void makeWidthEven(int);
	void makeHeightEven(int);

	void defaultAOI();
};
