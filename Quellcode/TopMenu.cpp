#include "TopMenu.h"




TopMenu::TopMenu()
{
	scaleFactor = 1;
	startButton = new QPushButton("");
	startButton->setMaximumWidth(150);
	startButton->setMinimumWidth(150);
	
	dirChooserButton = new QPushButton("Choose a Directory");
	dirChooserButton->setMaximumWidth(150);
	inZoomButton = new QPushButton("+");
	inZoomButton->setMaximumWidth(80);
	inZoomButton->setMinimumWidth(80);
	outZoomButton = new QPushButton("-");
	outZoomButton->setMaximumWidth(80);
	outZoomButton->setMinimumWidth(80);
	
	startState = new QState();
	startState->assignProperty(startButton, "text", "Start");
	startState->setObjectName("stop");
	
	stopState = new QState();
	stopState->assignProperty(startButton, "text", "Stop");
	stopState->setObjectName("start");

	
	startState->addTransition(startButton, SIGNAL(clicked()), stopState);
	
	stopState->addTransition(startButton, SIGNAL(clicked()), startState);
	
	startStopToggle.addState(startState);
	startStopToggle.addState(stopState);
	startStopToggle.setInitialState(startState);
	startStopToggle.start();



	//on / of switch
	embedButton = new QPushButton("");
	embedButton->setMinimumWidth(80);
	//StateMachine for embedded view
	off = new QState();
	off->assignProperty(embedButton, "text", "<>");
	off->setObjectName("off");

	on = new QState();
	on->setObjectName("on");
	on->assignProperty(embedButton, "text", "[ ]");
	saveButton = new QPushButton("Single Shot");
	saveButton->setMaximumWidth(150);
	saveButton->setMinimumWidth(150);
	
	off->addTransition(embedButton, SIGNAL(clicked()), on);
	on->addTransition(embedButton, SIGNAL(clicked()), off);

	Embedbtnswitch.addState(off);
	Embedbtnswitch.addState(on);
	Embedbtnswitch.setInitialState(on);
	Embedbtnswitch.start();
	
	spacer1 = new QWidget();
	spacer1->setMaximumWidth(300);
	spacer1->setMinimumWidth(250);
	spacer2 = new QWidget();
	spacer2->setMaximumWidth(400);
	spacer2->setMinimumWidth(300);


	topMenuBar = new QHBoxLayout;
	topMenuBar->addWidget(dirChooserButton);
	topMenuBar->addWidget(spacer1);
	topMenuBar->addWidget(saveButton);
	topMenuBar->addWidget(startButton);
	topMenuBar->addWidget(spacer2);
	topMenuBar->addWidget(inZoomButton);
	topMenuBar->addWidget(outZoomButton);
	topMenuBar->addWidget(embedButton);
	topMenuBar->setAlignment(Qt::AlignLeft);

	this->setLayout(topMenuBar);
	this->setMaximumHeight(80);

	dialog = new QFileDialog();


	//connections
	connect(inZoomButton, SIGNAL(clicked()), this, SLOT(zoomIn()));
	connect(outZoomButton, SIGNAL(clicked()), this, SLOT(zoomOut()));
	QObject::connect(dirChooserButton, SIGNAL(clicked()), this, SLOT(openDialog()));
	
}

//-----------GETTERS AND SETTERS------------------
QString TopMenu::getSavePath() { return savePath; }
QPushButton* TopMenu::getDirChooserBtn() { return dirChooserButton; }
QPushButton * TopMenu::getSaveBtn() { return saveButton; }
QPushButton* TopMenu::getStartButton() { return startButton; }
bool TopMenu::getEmbedbtnState() {return (*embedButton).text() == "[ ]";}
bool TopMenu::getStartbtnState() { return (*startButton).text() == "Start"; }
QState* TopMenu::getStopState() { return stopState; }
QState* TopMenu::getStartState() { return startState; }


//----------SLOTS-------------------
void TopMenu::zoomIn() {
	if (scaleFactor <= 2)
	scaleFactor = scaleFactor + 0.1;
	qDebug() << scaleFactor;
	return;
}
void TopMenu::zoomOut() {
	if (scaleFactor >= 0.2) { scaleFactor = scaleFactor - 0.1; }
	qDebug() << scaleFactor;
	return;
}
void TopMenu::openDialog() {
	qDebug() << "openDialog method";
	savePath = QFileDialog::getExistingDirectory(dialog,
		tr("Open Directory"), "D:/", QFileDialog::DontUseNativeDialog | QFileDialog::DontResolveSymlinks);
	qDebug() << savePath;
}
//methods
void TopMenu::debounce() {
	QTime dieTime = QTime::currentTime().addSecs(1);
	while (QTime::currentTime() < dieTime)
		QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

TopMenu::~TopMenu()
{
}


