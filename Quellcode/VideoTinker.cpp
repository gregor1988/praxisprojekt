#include "VideoTinker.h"

using namespace cv;
using namespace std;

VideoTinker::VideoTinker()
	: QThread()
{
	write = false;
	close = false;
	modus = START;
	sampleNumber = 1;
	fcc = CV_FOURCC('P', 'I', 'M', '1');
	// 'R', 'V', '2', '4' ----pylomprogramm
	//  CV_FOURCC('D', 'I', 'V', '5');
	//  CV_FOURCC('P', 'I', 'M', '1') = MPEG - 1 codec
	//	CV_FOURCC('M', 'J', 'P', 'G') = motion - jpeg codec
	//	CV_FOURCC('M', 'P', '4', '2') = MPEG - 4.2 codec
	//	CV_FOURCC('D', 'I', 'V', '3') = MPEG - 4.3 codec
	//	CV_FOURCC('D', 'I', 'V', 'X') = MPEG - 4 codec
	//	CV_FOURCC('U', '2', '6', '3') = H263 codec
	//	CV_FOURCC('I', '2', '6', '3') = H263I codec
	//	CV_FOURCC('F', 'L', 'V', '1') = FLV1 codec
}
void VideoTinker::writeFrame(QImage img) {
	if (modus == RECORDING) {
		qFrame = img.rgbSwapped();
	}
	else if (modus == SAMPLING) {
		qFrame = img;
	}
	write = true;
	bits = qFrame.bits();
	steps = qFrame.bytesPerLine();
	
}

void VideoTinker::endVideo() {
	close = true;
	modus = END;
}

void VideoTinker::run() {
	while (modus != END ) {
		QDate *date;
		QString currentDate = date->currentDate().toString();
		QTime *time;
		QString currentTime = time->currentTime().toString();
		currentDate.append("_");
		currentDate.append(currentTime);
		currentDate.replace(":", "-");
		currentDate.replace("", "_");

		if (modus == SAMPLING) { writeSampleSerie(currentDate); }
		else if (modus == RECORDING) { writeVideo(currentDate); }
	}
}

void VideoTinker::writeVideo(QString currentDate){
	path.append("/Video_");
	path.append(currentDate);
	path.append("_.avi");
	cv::VideoWriter writer;
	cv::Size frameSize(static_cast<int>(width), static_cast<int>(height));
	writer.open(path.toStdString(), fcc, fps, frameSize, true);

	while (close != true) {
		if (write) {
			write = false;
			writer.write(cv::Mat(qFrame.height(), qFrame.width(), CV_8UC3, qFrame.bits(), qFrame.bytesPerLine()));
		}
		waitKey(1);
	}
	close = false;
	writer.release();
}

void VideoTinker::writeSampleSerie(QString currentDate){
	
	while (close != true) {
		if (write == true) {
			write = false;
			QString qpath = path;
			qpath.append("/_");
			qpath.append(QString::number(sampleNumber));
			qpath.append("_Sample_");
			qpath.append(currentDate);
			qpath.replace(" ", "_");
			qpath.append("_.jpg");
			qFrame.save(qpath, "JPEG");
			sampleNumber++;
		}
		
	}
	close = false;
	sampleNumber = 1;
}


VideoTinker::~VideoTinker()
{
}
