#include "FieldOfView.h"



FieldOfView::FieldOfView()
{
	
	
	//int imageWidth = 800;
	//int imageHeight = 600;
	//int bytesPerPixel = 4; // 4 for RGBA, 3 for RGB
	//int format = QImage::Format_ARGB32; // this is the pixel format - check Qimage::Format enum type for more options
	//QImage image(yourData, imageWidth, imageHeight, imageWidth * bytesPerPixel, format);

	
	//label->setPixmap(*map);
}


FieldOfView::~FieldOfView()
{
}

void FieldOfView::run() {

	
	// Before using any pylon methods, the pylon runtime must be initialized.
	PylonInitialize();
	// Start the grabbing of c_countOfImagesToGrab images.
	// The camera device is parameterized with a default configuration which
	// sets up free-running continuous acquisition.

	CInstantCamera cam(CTlFactory::GetInstance().CreateFirstDevice());

	GenApi::INodeMap&nodeMap = cam.GetNodeMap();

	cam.Open();

	height = nodeMap.GetNode("Height");
	width = nodeMap.GetNode("Width");

	//specify output pixelformat
	PylonToOpencvConverter.OutputPixelFormat = PixelType_BGR8packed;

	cam.StartGrabbing();


	// Camera.StopGrabbing() is called automatically by the RetrieveResult() method
	// when c_countOfImagesToGrab images have been retrieved.
	while (cam.IsGrabbing())
	{
		// Wait for an image and then retrieve it. A timeout of 5000 ms is used.
		cam.RetrieveResult(5000, ptrGrabResult, TimeoutHandling_ThrowException);
		if (ptrGrabResult->GrabSucceeded())
		{
			//convert image data to PylonImage
			PylonToOpencvConverter.Convert(pylonImage, ptrGrabResult);
			//convert pylon image to OpenCV image
			OpencvImage = cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3, (uint8_t *)pylonImage.GetBuffer());
			//create OpenCV window
			namedWindow("OpenCV Window", CV_WINDOW_NORMAL);
			//Display current image in OpenCV window
			imshow("OpenCV Window", OpencvImage);
			this->qtImage= QImage((uchar*)OpencvImage.data, OpencvImage.cols, OpencvImage.rows, QImage::Format_RGB32);
			this->label->setPixmap(pixmap->fromImage(qtImage));
			waitKey(1);
		}
	}


};

