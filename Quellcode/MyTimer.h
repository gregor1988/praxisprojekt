#pragma once
#include <QtCore>
#include <QDebug>
#include <QTime>


class MyTimer : public QObject
{
	Q_OBJECT

public:
	MyTimer();
	~MyTimer();
	void startTimer(int,int);
	QTimer* getTimer();
	int getStart();
	int getStop();
	int getIncrement();
	void incStart(int);
	void reset();
private:
	QTimer *timer;
	QTime *time;
	int start;
	int increment;
	int stop;

};