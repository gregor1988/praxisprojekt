#pragma once

#include <QLabel>
#include <QPixmap>
#include <QImage>
#include <QThread>
#include <QtCore>
#include <QPainter>
#include "qtimer.h"
#include "MetaDataContainer.h"
#include "VideoTinker.h"
#include "MyTimer.h"
#include "AOIparameterContainer.h"

#include <pylon/PylonIncludes.h>




//Include OpenCV api
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <QThread>
#include <QDebug>
// Settings for using Basler USB cameras.
#include <pylon/usb/BaslerUsbInstantCamera.h>
typedef Pylon::CBaslerUsbInstantCamera Camera_t;

using namespace Basler_UsbCameraParams;

using namespace Pylon;
using namespace std;

class CameraThread : public QThread
{
	Q_OBJECT

public:
	CameraThread();
	~CameraThread();
	enum aqHandling { START, STOP, SET, REC, END, SETGAMMA, SETXOFFSET, SETYOFFSET, SETWIDTH, SETHEIGHT, SETEXPOSURETIME, SETAOI, DEFAULTAOI,
		SETEXPOSUREAUTO };
	aqHandling aqHandl;
	//overwritten QThread function -- runs when QThread::start(); was called
	void run();
	//initialize values because there is no saving
	void initVals();
	//setters
	void jobDoneAt();
	void setTimeDecrement(int);
	void setTimeStop(int);
	void toggleTakeShot();
	void toggleOverlay();
	void toggleSingleShotOverlay();
	
	//getters
	QImage getImage();
private:
	//settingValues
	int gammaValue;
	int64_t offsetXval;
	int64_t offsetYval;
	int64_t widthVal;
	int64_t heightVal;
	int64_t exposureVal;
	bool autoExposure;
	bool AOI;

	//Images
	CPylonImage pylonImage;
	cv::Mat OpencvImage;
	QImage qtImage;
	QPixmap pixMap;

	bool singleShotOverlay;
	bool takeShot;
	bool overlay;
	int timeDecrement;
	int timeStop;
	QString intToQString(int);
	QString jobDone;

	void continousAquisition();
	void calculateRemainingTime();
	
	//entprellen
	void debounce(int);



	//settings
	void gamma();
	void setXOffset();
	void setYOffset();
	void setWidth();
	void setHeight();
	void setExposureTime();
	void setAutoExposure();
	void setAOI();
	void setDefaultAOI();

signals:
	void emitPixMap(QPixmap);
	void emitQImage(QImage);
	void emitFrame(QImage);
	void emitAutoExposureTime(int);

public slots:	//incomming signals
	//slots for settings
	void setGammaOnNodeMap(int);
	void setOffsetXOnNodeMap(int);
	void setOffsetYOnNodeMap(int);
	void setWidthOnNodeMap(int);
	void setHeightOnNodeMap(int);
	void setExposureTimeOnNodeMap(int);
	void setAutoExposureOnNodeMap();
	void setAOIOnNodeMap(AOIparameterContainer*);
	void setDefaultAOIOnNodeMap();
};
