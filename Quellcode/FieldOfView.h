#pragma once
#include <QLabel>
#include <QPixmap>
#include <QImage>
#include <QThread>
#include <QtCore>

#include <pylon/PylonIncludes.h>
#ifdef PYLON_WIN_BUILD
#    include <pylon/PylonGUI.h>
#endif

//Include OpenCV api
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>


using namespace Qt;
using namespace Pylon;
using namespace cv;

class FieldOfView : public QThread
{
	Q_OBJECT

public:
	FieldOfView();
	QLabel *label;
	QPixmap *pixmap;

	void run();

	~FieldOfView();
private:
	QPixmap *map;

	CGrabResultPtr ptr;

	//NodeMap has parameters of the camera
	//GenApi::INodeMap& nodeMap;
	
	//pointers to camra parameters
	GenApi::CIntegerPtr width;
	GenApi::CIntegerPtr height;

	//converters
	CImageFormatConverter PylonToOpencvConverter;
	
	//Images
	CPylonImage pylonImage;
	Mat OpencvImage;
	QImage qtImage;

	// This smart pointer will receive the grab result data.
	CGrabResultPtr ptrGrabResult;
	

};

