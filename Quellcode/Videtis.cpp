#include "Videtis.h"



using namespace std;

Videtis::Videtis()
{
	//Components
	mainWindow = new QWidget();
	mainWindow->setWindowTitle("Videtis");
	topMenu = new TopMenu();
	settings = new SettingsMenu();
	imageView = new QLabel();
	imageView->setAlignment(AlignTop);
	imageViewScrollArea = new QScrollArea();
	imageViewScrollArea->setWidget(imageView);
	imageViewScrollArea->setWidgetResizable(true);
	
	splitter = new QSplitter();
	splitter->addWidget(settings->menubar);
	splitter->addWidget(imageViewScrollArea);
	box = new QVBoxLayout();
	box->addWidget(topMenu);
	box->addWidget(splitter);

	mainWindow->setLayout(box);
	setCentralWidget(mainWindow);

	timer = new MyTimer();
	writer = new VideoTinker();


//Signals from CameraThread
	camThread = new CameraThread();
	connect(camThread, SIGNAL(emitPixMap(QPixmap)), this, SLOT(pixmapRecieved(QPixmap)));
	connect(camThread, SIGNAL(emitQImage(QImage)), this, SLOT(saveImage(QImage)));
	
//Signals to CameraThread
	//connections from TopMenu to CameraThread
		//aufnahme
	connect(topMenu->getStartButton(), SIGNAL(clicked()), settings->getRecOptions(), SLOT(sendMetaData()));
	connect(settings->getRecOptions(), SIGNAL(emitMetaData(MetaDataContainer*)), this, SLOT(startVideo(MetaDataContainer*)));
	topMenu->getStopState()->addTransition(this, SIGNAL(timerStopped()), topMenu->getStartState());
	topMenu->getStartState()->addTransition(this, SIGNAL(stopState()), topMenu->getStopState());
	topMenu->getStopState()->addTransition(this, SIGNAL(stopped()), topMenu->getStopState());
	connect(timer->getTimer(), SIGNAL(timeout()), this, SLOT(fire()));
	connect(camThread, SIGNAL(emitFrame(QImage)), writer, SLOT(writeFrame(QImage)));
		//samples

		//bild speichern
	connect(topMenu->getSaveBtn(), SIGNAL(clicked()), this, SLOT(saveImagebtnClicked()));
	//connections from SettingsMenu to CameraThread
		//Gamma
	connect(settings->getGammaSetting()->getSpinner(), SIGNAL(valueChanged(int)), camThread,SLOT(setGammaOnNodeMap(int)));
		//AOI
	connect(settings->getAOISetting()->getOffsetXSpinner(), SIGNAL(valueChanged(int)), camThread, SLOT(setOffsetXOnNodeMap(int)));
	connect(settings->getAOISetting()->getOffsetYSpinner(), SIGNAL(valueChanged(int)), camThread, SLOT(setOffsetYOnNodeMap(int)));
	connect(settings->getAOISetting()->getWidthSpinner(), SIGNAL(valueChanged(int)), camThread, SLOT(setWidthOnNodeMap(int)));
	connect(settings->getAOISetting()->getHeightSpinner(), SIGNAL(valueChanged(int)), camThread, SLOT(setHeightOnNodeMap(int)));
	connect(imageViewScrollArea, SIGNAL(mousePressEvent(QMouseEvent  *)), this, SLOT(draw(QMouseEvent  *)));
	connect(this, SIGNAL(sendAOIparameterContainer(AOIparameterContainer*)), camThread, SLOT(setAOIOnNodeMap(AOIparameterContainer*)));
	connect(settings->getAOISetting(), SIGNAL(defaultBtnClicked()), camThread, SLOT(setDefaultAOIOnNodeMap()));
	connect(settings->getAOISetting(), SIGNAL(defaultBtnClicked()), this, SLOT(resetAOI()));
		//ExposureTime
	connect(settings->getExposureTimeOptions()->getSpinner(), SIGNAL(valueChanged(int)), camThread, SLOT(setExposureTimeOnNodeMap(int)));
	connect(settings->getExposureTimeOptions()->getCheckBox(), SIGNAL(clicked()), camThread, SLOT(setAutoExposureOnNodeMap()));
	connect(camThread, SIGNAL(emitAutoExposureTime(int)), settings->getExposureTimeOptions()->getSpinner(), SLOT(setValue(int)));
	camThread->start();
}
void Videtis::startVideo(MetaDataContainer* metaData) {
	
	if (topMenu->getSavePath() == NULL)
	{
		
		QMessageBox Msgbox;
		Msgbox.setText("Choose a directory...");
		Msgbox.exec();
		emit stopState();
		return;
	}
	else if(topMenu->getStartbtnState()) {
		if (settings->getRecOptions()->getFpsSpinner()->isEnabled()) {
			writer->modus = VideoTinker::RECORDING;  //start video
		}else{ writer->modus = VideoTinker::SAMPLING; }  //start sampling
		writer->path = topMenu->getSavePath();
		writer->height = camThread->getImage().height();
		writer->width = camThread->getImage().width();
		writer->fps = (*metaData).getPlaybackfps();
		writer->start();
		timer->startTimer((*metaData).getRecordfps(), (*metaData).getStopTime());
		camThread->setTimeDecrement(timer->getIncrement());
		camThread->setTimeStop(timer->getStop());
		camThread->jobDoneAt();
		camThread->toggleOverlay();
		
		//disable settings
		settings->disableAllOptions();
	}
	else {
		emit stopped();
		writer->endVideo();
		timer->reset();
		camThread->toggleOverlay();
		//enable settings
		settings->enableAllOptions();
	}qDebug() << topMenu->getStartbtnState();
}
void Videtis::pixmapRecieved(QPixmap map) {
	
	if (topMenu->getEmbedbtnState()) {
		(*topMenu).scaleFactor = 1;
		imageViewScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		imageViewScrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		imageView->setScaledContents(false);
		pixmapWidth = (*imageViewScrollArea).width();
		pixmapHeight = (*imageViewScrollArea).height();
		//draw
		
		QPixmap map2 = map.scaled(int((*topMenu).scaleFactor*pixmapWidth), int((*topMenu).scaleFactor*pixmapHeight), Qt::KeepAspectRatio);
		if (boolDraw) {
			QPainter painterr(&map2);
			painterr.setRenderHint(QPainter::Antialiasing);
			QPen pen = QPen(Qt::darkGreen);
			pen.setWidth(10);
			painterr.setPen(pen);
			QRect rectangle = QRect(x1, y1, x2 - x1, y2 - y1);
			painterr.drawRect(rectangle);
		}
		imageView->setPixmap(map2);
	}
	else {
		imageViewScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
		imageViewScrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
		imageViewScrollArea->setWidgetResizable(true);
		imageView->setScaledContents(false);
		
		imageView->setPixmap(map.scaled(int((*topMenu).scaleFactor*pixmapWidth), int((*topMenu).scaleFactor*pixmapHeight), Qt::KeepAspectRatio));
	}
}
void Videtis::debounce(int ms) {
	QTime dieTime = QTime::currentTime().addMSecs(ms);
	while (QTime::currentTime() < dieTime)
		QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}
void Videtis::saveImagebtnClicked()
{
	camThread->toggleTakeShot();
	debounce(1000);
	camThread->toggleSingleShotOverlay();
}
void Videtis::saveImage(QImage image){
	if (topMenu->getSavePath() == NULL)
	{

		QMessageBox Msgbox;
		Msgbox.setText("Choose a directory...");
		Msgbox.exec();
		return;
	}
	QDate *date;
	QString currentDate = date->currentDate().toString();
	QTime *time;
	QString currentTime = time->currentTime().toString();
	currentDate.append(" ");
	currentDate.append(currentTime);
	currentDate.replace(":", "-");
	QString qtPath = topMenu->getSavePath().append("/Image_");
	qtPath.append(currentDate);
	qtPath.append("_.jpg");
	qtPath.replace(" ", "-");
	image.save(qtPath, "JPEG");
}
void Videtis::fire() {
	camThread->aqHandl = CameraThread::REC;
	timer->incStart(timer->getIncrement());

	
	if (timer->getStart() >= timer->getStop()) {
		emit timerStopped();
		writer->endVideo();
		timer->reset();
		camThread->toggleOverlay();
		//enable settings
		settings->enableAllOptions();
	}
}

void Videtis::draw(QMouseEvent  *) {
	
}

void Videtis::mousePressEvent(QMouseEvent *event)
{
	if (event->x() > settings->menubar->width() - 18 && event->y() > topMenu->height() - 20){
		if (event->button() == Qt::LeftButton) {
			boolDraw = true;

			x1 = event->x() - settings->menubar->width() - 18;
			y1 = event->y() - topMenu->height() - 20;

			x2 = x1;
			y2 = y1;
		}
	}
}

void Videtis::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && boolDraw)
	x2 = event->x() - settings->menubar->width() - 18;
	y2 = event->y() - topMenu->height() - 20;
}

void Videtis::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && boolDraw) {
		boolDraw = false;
		x2 = event->x() - settings->menubar->width() - 18;
		y2 = event->y() - topMenu->height() - 20;
		AOIparameterContainer* container = new AOIparameterContainer();
		float ratio = 1.33;
		double widthtmp;
		int heighttmp;
		
		if (imageView->width()*1.0 / imageView->height()*1.0 < ratio) { widthtmp = imageView->width(); heighttmp = int(widthtmp / ratio); }
		else { heighttmp = imageView->height(); widthtmp = int(heighttmp * ratio); }
		if (x1 > x2) { int temp = x1; x1 = x2; x2 = temp; }
		if (y1 > y2) { int temp = y1; y1 = y2; y2 = temp; }
		if (event->x() > settings->menubar->width() - 18 && event->y() > topMenu->height() - 20) {
			container->setOffsetXval(int(float(x1) / widthtmp *2526.0));
			container->setOffsetYval(int(float(y1) / heighttmp *1878.0));
			container->setWidthVal(int(double(x2 - x1)) / widthtmp *2590.0);
			container->setHeightVal(int(float(y2 - y1) / heighttmp *1942.0));
			emit sendAOIparameterContainer(container);
			settings->getAOISetting()->getWidthSpinner()->setValue(int(double(x2 - x1)) / double(widthtmp)*2590.0);   //(x2-x1)/widthtmp*2590
			settings->getAOISetting()->getHeightSpinner()->setValue(int(float(y2 - y1) / heighttmp *1942.0)); //(y2-y1)/heighttmp*1942.0
			settings->getAOISetting()->getOffsetXSpinner()->setValue(int(x1 / float(widthtmp) *2526.0)); //widthtmp/x1*2526
			settings->getAOISetting()->getOffsetYSpinner()->setValue(int(float(y1) / heighttmp *1878.0)); //y1/heighttmp*1878.0
		}
	}
}


void Videtis::resetAOI() {
	debounce(100);
	settings->getAOISetting()->getOffsetXSpinner()->setValue(0);
	settings->getAOISetting()->getOffsetYSpinner()->setValue(0);
	settings->getAOISetting()->getWidthSpinner()->setValue(2590);
	settings->getAOISetting()->getHeightSpinner()->setValue(1942);
 
}