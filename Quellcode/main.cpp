#include "Videtis.h"
#include <QtWidgets/QApplication>
#include <QFile>

#include <pylon/PylonIncludes.h>
#ifdef PYLON_WIN_BUILD
#    include <pylon/PylonGUI.h>
#endif
using namespace std;
using namespace Pylon;

int main(int argc, char *argv[])
{
	
	QApplication a(argc, argv);

	QFile file("StyleSheet.qml");
	if (file.open(QFile::ReadOnly)) {
		QString styleSheet = QLatin1String(file.readAll());
		a.setStyleSheet(styleSheet);
	}

	Videtis *v = new Videtis();
	v->setWindowTitle("Videtis");
	

	v->showMaximized();
	return a.exec();

}
