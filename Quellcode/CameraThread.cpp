#include "CameraThread.h"
using namespace cv;
using namespace Qt;

CameraThread::CameraThread()
	: QThread()
{
	initVals();
	aqHandl = START;
	takeShot = false;
	overlay = false;
	singleShotOverlay = false;
}

QImage CameraThread::getImage()
{
	return qtImage;
}
//Thread
void CameraThread::run()
{
	while (aqHandl != END) {
		if (aqHandl == START || aqHandl == REC) {
			continousAquisition();
		}
		else if (aqHandl == SETGAMMA) {gamma();}
		else if (aqHandl == SETXOFFSET) { setXOffset();  }
		else if (aqHandl == SETYOFFSET) { setYOffset(); }
		else if (aqHandl == SETWIDTH) { setWidth(); }
		else if (aqHandl == SETHEIGHT) { setHeight(); }
		else if (aqHandl == SETEXPOSURETIME) { setExposureTime(); }
		else if (aqHandl == SETEXPOSUREAUTO) { setAutoExposure(); }
		else if (aqHandl == SETAOI) { setAOI(); }
		else if (aqHandl == DEFAULTAOI) { setDefaultAOI(); }


	}
}

//getTheImageOnScreen
void CameraThread::continousAquisition() {
	// Prevent nasty crashes
	QMutex mutex;
	mutex.lock();
	// Before using any pylon methods, the pylon runtime must be initialized.
	PylonInitialize();

	// Start the grabbing of c_countOfImagesToGrab images.
	// The camera device is parameterized with a default configuration which
	// sets up free-running continuous acquisition.
	try
	{
		// Only look for cameras supported by Camera_t.
		CDeviceInfo info;
		info.SetDeviceClass(Camera_t::DeviceClass());

		// Create an instant camera object with the first found camera device matching the specified device class.
		Camera_t cam(CTlFactory::GetInstance().CreateFirstDevice(info));
		cam.Open();
		//specify output pixelformat
		CImageFormatConverter PylonToOpencvConverter;
		PylonToOpencvConverter.OutputPixelFormat = PixelType_RGB8packed;

		cam.StartGrabbing();
		CGrabResultPtr ptrGrabResult;

		// Camera.StopGrabbing() is called automatically by the RetrieveResult() method
		// when c_countOfImagesToGrab images have been retrieved.
		while (aqHandl == START || aqHandl == REC)
		{
			
			// Wait for an image and then retrieve it. A timeout of 5000 ms is used.
			cam.RetrieveResult(5000, ptrGrabResult, TimeoutHandling_ThrowException);
			if (ptrGrabResult->GrabSucceeded())
			{
				//convert image data to PylonImage
				PylonToOpencvConverter.Convert(pylonImage, ptrGrabResult);
				//convert pylon image to OpenCV image
				OpencvImage = cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3, (uint8_t *)pylonImage.GetBuffer());
				//cast mat image to QImage
				qtImage = QImage((unsigned char*)OpencvImage.data, OpencvImage.cols, OpencvImage.rows, OpencvImage.step, QImage::Format_RGB888);
				Mat displayImage = OpencvImage.clone();
				//frame senden
				if (aqHandl == REC) {
					emit emitFrame(qtImage); aqHandl = START;
				}
				//cast QImage to QPixmap and emit
				pixMap = (QPixmap::fromImage(QImage((unsigned char*)displayImage.data, displayImage.cols, displayImage.rows, displayImage.step, QImage::Format_RGB888)));

				if(overlay){
					QPainter painter(&pixMap);
					QFont font = painter.font();
					font.setPixelSize((pixMap.width()) / 20);
					font.setBold(true);
					painter.setFont(font);
					painter.setPen(red);
					painter.drawText(QPoint((pixMap.width()) / 20, pixMap.height() / 10), "Rec");
					painter.setPen(white);
					painter.drawText(QPoint((pixMap.width()) / 20, pixMap.height() / 2), "Job done at");
					painter.drawText(QPoint((pixMap.width()) / 20, pixMap.height() / 1.5), jobDone);
				}
				
				//save single shot
				if (takeShot) {
					takeShot = false;
					singleShotOverlay = true;
					emit emitQImage(qtImage);
				}
				if (singleShotOverlay) {
					QPainter painter(&pixMap);
					QFont font = painter.font();
					font.setPixelSize((pixMap.width()) / 20);
					font.setBold(true);
					painter.setFont(font);
					painter.setPen(green);
					painter.drawText(QPoint((pixMap.width()) / 4, pixMap.height() / 2), "Saved");
				}
				//emit pixmap
				emit emitPixMap(pixMap);
				waitKey(1);
			}
		}
		mutex.unlock();
	}
	catch (const GenericException &e)
	{
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
	}
	PylonTerminate();
	
}
//setters for NodeMap
void CameraThread::setGammaOnNodeMap(int gammaVal) {
		aqHandl = SETGAMMA;
		gammaValue = gammaVal;
}
void CameraThread::setOffsetXOnNodeMap(int val){
	if (!AOI) {
		offsetXval = val;
		aqHandl = SETXOFFSET;
	}
}
void CameraThread::setOffsetYOnNodeMap(int val){
	if (!AOI) {
		offsetYval = val;
		aqHandl = SETYOFFSET;
	}
}
void CameraThread::setWidthOnNodeMap(int val){
	if (!AOI) {
		widthVal = val;
		aqHandl = SETWIDTH;
	}
}
void CameraThread::setHeightOnNodeMap(int val){
	if (!AOI) {
		heightVal = val;
		aqHandl = SETHEIGHT;
	}
}
void CameraThread::setExposureTimeOnNodeMap(int val) {
		aqHandl = SETEXPOSURETIME;
		exposureVal = val;
}
void CameraThread::setAutoExposureOnNodeMap() {
	aqHandl = SETEXPOSUREAUTO;
	autoExposure = !autoExposure;
}

void CameraThread::setAOIOnNodeMap(AOIparameterContainer *c)
{
	aqHandl = SETAOI;
	AOI = true;
	offsetXval = c->getOffsetXval();
	offsetYval= c->getOffsetYval();
	widthVal = c->getWidthVal();
	heightVal = c->getHeightVal();
}

void CameraThread::setDefaultAOIOnNodeMap() {
	AOI = true;
	offsetXval = 2526;
	offsetYval = 1878;
	widthVal = 2590;
	heightVal = 1942;
	aqHandl = DEFAULTAOI;
}

CameraThread::~CameraThread()
{
}

void CameraThread::debounce(int ms) {
	QTime dieTime = QTime::currentTime().addMSecs(ms);
	while (QTime::currentTime() < dieTime)
		QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

void CameraThread::toggleTakeShot() { takeShot = !takeShot; }
void CameraThread::toggleOverlay() { overlay = !overlay; }
void CameraThread::setTimeDecrement(int n) { timeDecrement = n; }
void CameraThread::setTimeStop(int n) { timeStop = n; }

void CameraThread::calculateRemainingTime(){
	timeStop = (timeStop - timeDecrement);
}

void CameraThread::initVals(){
	// Before using any pylon methods, the pylon runtime must be initialized. 
	PylonInitialize();
	try
	{
		// Only look for cameras supported by Camera_t.
		CDeviceInfo info;
		info.SetDeviceClass(Camera_t::DeviceClass());

		// Create an instant camera object with the first found camera device matching the specified device class.
		Camera_t camera(CTlFactory::GetInstance().CreateFirstDevice(info));

		// Open the camera for accessing the parameters.
		camera.Open();
		
		exposureVal = camera.ExposureTime.GetValue();
		autoExposure = true;
		camera.ExposureAuto.SetValue(ExposureAuto_Continuous);
		camera.Gamma.SetValue(1);
		gammaValue =10;
		camera.OffsetX.SetValue(0);
		offsetXval = 0;
		camera.OffsetY.SetValue(0);
		offsetYval = 0;
		camera.Width.SetValue(2590);
		widthVal = 2590;
		camera.Height.SetValue(1942);
		heightVal = 1942;
		
		// Close the camera.
		camera.Close();

	}
	catch (const GenericException &e) {
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
	}
	PylonTerminate();
}

QString CameraThread::intToQString(int number) {
	return QString::number(number);
}

void CameraThread::jobDoneAt() {
	QDateTime date;
	
	jobDone=date.currentDateTime().addMSecs(timeStop).toString();
	
};

void CameraThread::gamma(){
	// Before using any pylon methods, the pylon runtime must be initialized. 
	PylonInitialize();

	try
	{
		// Only look for cameras supported by Camera_t.
		CDeviceInfo info;
		info.SetDeviceClass(Camera_t::DeviceClass());

		// Create an instant camera object with the first found camera device matching the specified device class.
		Camera_t camera(CTlFactory::GetInstance().CreateFirstDevice(info));

		// Open the camera for accessing the parameters.
		camera.Open();
		//assert Value is in parameter range
		if (gammaValue / 10.0 >= 1.0 && gammaValue / 10.0 <= 3.9) {
			camera.Gamma.SetValue(gammaValue / 10.0);
		}
		aqHandl = START;
		// Close the camera.
		camera.Close();
	}
	catch (const GenericException &e) {
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
	}
	PylonTerminate();
}
void CameraThread::setXOffset(){
	// Before using any pylon methods, the pylon runtime must be initialized. 
	PylonInitialize();
	int64_t maxWidth = 2590;
	try
	{
		// Only look for cameras supported by Camera_t.
		CDeviceInfo info;
		info.SetDeviceClass(Camera_t::DeviceClass());

		// Create an instant camera object with the first found camera device matching the specified device class.
		Camera_t camera(CTlFactory::GetInstance().CreateFirstDevice(info));

		// Open the camera for accessing the parameters.
		camera.Open();
		//assert Value is in parameter range
		if (maxWidth - offsetXval >= camera.Width.GetValue()) {
			camera.OffsetX.SetValue(offsetXval);
			if (camera.Width.GetValue() == widthVal) {
				aqHandl = START;
			}
			else{ aqHandl = SETWIDTH; }
		}
		else { aqHandl = SETWIDTH; }
		camera.Close();
	}
	catch (const GenericException &e) {
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
	}
	PylonTerminate();
}
void CameraThread::setYOffset(){
	int64_t maxHeight = 1942;
	// Before using any pylon methods, the pylon runtime must be initialized. 
	PylonInitialize();

	try
	{
		// Only look for cameras supported by Camera_t.
		CDeviceInfo info;
		info.SetDeviceClass(Camera_t::DeviceClass());

		// Create an instant camera object with the first found camera device matching the specified device class.
		Camera_t camera(CTlFactory::GetInstance().CreateFirstDevice(info));

		// Open the camera for accessing the parameters.
		camera.Open();
		//assert Value is in parameter range
		if (maxHeight - offsetYval >= camera.Height.GetValue()) {
			camera.OffsetY.SetValue(offsetYval);
			aqHandl = START;
			if(camera.Height.GetValue() == heightVal){ aqHandl = START; }
			else{ aqHandl = SETHEIGHT; }
		}
		else { aqHandl = SETHEIGHT; }
		camera.Close();
	}
	catch (const GenericException &e) {
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
	}
	// Close the camera.
	PylonTerminate();
	
}
void CameraThread::setWidth() {
	// Before using any pylon methods, the pylon runtime must be initialized. 
	int64_t maxWidth = 2590;
	PylonInitialize();
	try
	{
		// Only look for cameras supported by Camera_t.
		CDeviceInfo info;
		info.SetDeviceClass(Camera_t::DeviceClass());

		// Create an instant camera object with the first found camera device matching the specified device class.
		Camera_t camera(CTlFactory::GetInstance().CreateFirstDevice(info));

		// Open the camera for accessing the parameters.
		camera.Open();
		//assert Value is in parameter range
		if (maxWidth - camera.OffsetX.GetValue() >= widthVal) {
			camera.Width.SetValue(widthVal);
			if (camera.OffsetX.GetValue() == offsetXval) { aqHandl = START; }
			else { aqHandl = SETXOFFSET; }
		}
		else { aqHandl = SETXOFFSET; }
		camera.Close();
	}
	catch (const GenericException &e) {
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
	}
	PylonTerminate();
}
void CameraThread::setHeight(){
	// Before using any pylon methods, the pylon runtime must be initialized. 
	PylonInitialize();
	int64_t maxHeight = 1942;
	try
	{
		// Only look for cameras supported by Camera_t.
		CDeviceInfo info;
		info.SetDeviceClass(Camera_t::DeviceClass());

		// Create an instant camera object with the first found camera device matching the specified device class.
		Camera_t camera(CTlFactory::GetInstance().CreateFirstDevice(info));

		// Open the camera for accessing the parameters.
		camera.Open();
		//assert Value is in parameter range
		if (maxHeight - camera.OffsetY.GetValue() >= heightVal) {
			camera.Height.SetValue(heightVal);
			if (camera.OffsetY.GetValue()== offsetYval){ aqHandl = START; }
			else{ aqHandl = SETYOFFSET; }
		}
		else { aqHandl = SETYOFFSET; }
		camera.Close();
	}
	catch (const GenericException &e) {
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
	}
	PylonTerminate();

}
void CameraThread::setExposureTime(){

	// Before using any pylon methods, the pylon runtime must be initialized. 
	PylonInitialize();
	try
	{
		// Only look for cameras supported by Camera_t.
		CDeviceInfo info;
		info.SetDeviceClass(Camera_t::DeviceClass());

		// Create an instant camera object with the first found camera device matching the specified device class.
		Camera_t camera(CTlFactory::GetInstance().CreateFirstDevice(info));

		// Open the camera for accessing the parameters.
		camera.Open();
		//assert Value is in parameter range
		if (exposureVal >= 35 && exposureVal <= 9999990) {
			camera.ExposureTime.SetValue(exposureVal);
		}
		// Close the camera.
		camera.Close();

	}
	catch (const GenericException &e) {
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
	}
	aqHandl = START;
	PylonTerminate();
}
void CameraThread::setAutoExposure() {
	// Before using any pylon methods, the pylon runtime must be initialized. 
	PylonInitialize();

	try
	{
		// Only look for cameras supported by Camera_t.
		CDeviceInfo info;
		info.SetDeviceClass(Camera_t::DeviceClass());

		// Create an instant camera object with the first found camera device matching the specified device class.
		Camera_t camera(CTlFactory::GetInstance().CreateFirstDevice(info));

		// Open the camera for accessing the parameters.
		camera.Open();
		//assert Value is in parameter range
		if (autoExposure) {
			camera.ExposureAuto.SetValue(ExposureAuto_Continuous);
		}
		else { 
			exposureVal = camera.ExposureTime.GetValue();
			camera.ExposureAuto.SetValue(ExposureAuto_Off); emit emitAutoExposureTime(exposureVal); }
		// Close the camera.
		camera.Close();
	}
	catch (const GenericException &e) {
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
	}
	aqHandl = START;
}
void CameraThread::setAOI() {
	// Before using any pylon methods, the pylon runtime must be initialized. 
	PylonInitialize();
	try
	{
		// Only look for cameras supported by Camera_t.
		CDeviceInfo info;
		info.SetDeviceClass(Camera_t::DeviceClass());

		// Create an instant camera object with the first found camera device matching the specified device class.
		Camera_t camera(CTlFactory::GetInstance().CreateFirstDevice(info));

		// Open the camera for accessing the parameters.
		camera.Open();

		if(widthVal%2==0){ camera.Width.SetValue(widthVal); }
		else{
			if (widthVal % 2 != 0 && widthVal+1<2590){ camera.Width.SetValue(widthVal+1); }
			else{ camera.Width.SetValue(widthVal - 1); }
		}
		if (heightVal % 2 == 0){ camera.Height.SetValue(heightVal); }
		else {
			if (heightVal % 2 != 0 && heightVal + 1<1942) { camera.Height.SetValue(heightVal + 1); }
			else { camera.Height.SetValue(heightVal - 1); }
		}
		if (offsetXval % 2 == 0) { camera.OffsetX.SetValue(offsetXval); }
		else {
			if (offsetXval % 2 != 0 && offsetXval + 1<1942) { camera.OffsetX.SetValue(offsetXval + 1); }
			else { camera.OffsetX.SetValue(offsetXval - 1); }
		}
		if (offsetYval % 2 == 0) { camera.OffsetY.SetValue(offsetYval); }
		else {
			if (offsetYval % 2 != 0 && offsetYval + 1<1942) { camera.OffsetY.SetValue(offsetYval + 1); }
			else { camera.OffsetY.SetValue(offsetYval - 1); }
		}


		camera.Close();
	}
	catch (const GenericException &e) {
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
	}
	PylonTerminate();

	AOI = false;
	aqHandl = START;
}
void CameraThread::setDefaultAOI() {
	// Before using any pylon methods, the pylon runtime must be initialized. 
	PylonInitialize();
	try
	{
		// Only look for cameras supported by Camera_t.
		CDeviceInfo info;
		info.SetDeviceClass(Camera_t::DeviceClass());

		// Create an instant camera object with the first found camera device matching the specified device class.
		Camera_t camera(CTlFactory::GetInstance().CreateFirstDevice(info));

		// Open the camera for accessing the parameters.
		camera.Open();

		camera.OffsetX.SetValue(0);
		camera.OffsetY.SetValue(0);

		camera.Width.SetValue(2590); 
		camera.Height.SetValue(1942); 
		

		camera.Close();
	}
	catch (const GenericException &e) {
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
	}
	PylonTerminate();
	debounce(100);
	AOI = false;
	aqHandl = START;
	
}


void CameraThread::toggleSingleShotOverlay() {
	singleShotOverlay = !singleShotOverlay;
}
