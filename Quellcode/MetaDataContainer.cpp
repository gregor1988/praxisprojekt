#include "MetaDataContainer.h"

MetaDataContainer::MetaDataContainer(int sfps, int stop, int pfps,int sfpsindex,int stopindex)
	: QObject()
{
	recordfps = sfps;
	stopTime = stop;
	playbackfps = pfps;
	if (sfpsindex == 0) {
		recordFactor = 1;
	}
	else if (sfpsindex == 1) {
		recordFactor = 1000;
	}
	else if (sfpsindex == 2){
		recordFactor = 1000 * 60;
	}
	else if (sfpsindex == 3) {
		recordFactor = 1000 * 60 * 60;
	}
	
	if (stopindex == 0) {
		stopFactor = 1;;
	}
	else if (stopindex == 1) {
		stopFactor = 1000;
	}
	else if (stopindex == 2) {
		stopFactor = 1000 * 60;
	}
	else if (stopindex == 3) {
		stopFactor = 1000 * 60 * 60;
	}
	
}

MetaDataContainer::~MetaDataContainer()
{
}
//setters
void MetaDataContainer::setPath(QString str) { path = str; }
void MetaDataContainer::setHeight(int n) { height = n; }
void MetaDataContainer::setWidth(int n) { width = n; }
void MetaDataContainer::setRecordfps(int n) { recordfps = n; }
void MetaDataContainer::setPlaybackfps(int n) { playbackfps = n; }
void MetaDataContainer::setStopTime(int n) { stopTime = n; }
void MetaDataContainer::setRecordFactor(int n) { recordFactor = n; }
void MetaDataContainer::setStopFactor(int n) { stopFactor = n; }

//getters
QString MetaDataContainer::getPath() { return path; }
int MetaDataContainer::getRecordfps() { return recordfps*recordFactor; }
int MetaDataContainer::getHeight() { return height; }
int MetaDataContainer::getWidth() { return width; }
int MetaDataContainer::getPlaybackfps() { return playbackfps; }
int MetaDataContainer::getStopTime() { return stopTime*stopFactor; }