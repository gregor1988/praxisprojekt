#pragma once

#include <QObject>
#include <QImage>

class MetaDataContainer : public QObject
{
	Q_OBJECT

public:
	MetaDataContainer(int, int, int, int, int);
	

	void setPath(QString);
	void setHeight(int);
	void setWidth(int);
	void setRecordfps(int);
	void setPlaybackfps(int);
	void setStopTime(int);
	void setRecordFactor(int);
	void setStopFactor(int);

	QString getPath();
	int getRecordfps();
	int getHeight();
	int getWidth();
	int getPlaybackfps();
	int getStopTime();
	~MetaDataContainer();

private:
	QString path;
	int recordfps;
	int recordFactor;
	int stopTime;
	int stopFactor;
	int height;
	int width;
	int playbackfps;
};
