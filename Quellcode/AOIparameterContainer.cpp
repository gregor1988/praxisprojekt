#include "AOIparameterContainer.h"

AOIparameterContainer::AOIparameterContainer()
	: QObject()
{
}

AOIparameterContainer::~AOIparameterContainer()
{
}

void AOIparameterContainer::setOffsetXval(int64_t val)
{
	offsetXval = val;
}

void AOIparameterContainer::setOffsetYval(int64_t val)
{
	offsetYval = val;
}

void AOIparameterContainer::setWidthVal(int64_t val)
{
	widthVal = val;
}

void AOIparameterContainer::setHeightVal(int64_t val)
{
	heightVal = val;
}

int64_t AOIparameterContainer::getOffsetXval()
{
	return offsetXval;
}

int64_t AOIparameterContainer::getOffsetYval()
{
	return offsetYval;
}

int64_t AOIparameterContainer::getWidthVal()
{
	return widthVal;
}

int64_t AOIparameterContainer::getHeightVal()
{
	return heightVal;
}
