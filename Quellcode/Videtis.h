#pragma once

#include <QtCore>
#include <QObject>
#include "SettingsMenu.h"
#include "TopMenu.h"
#include "CameraThread.h"
#include "MetaDataContainer.h"
#include "AOIparameterContainer.h"
#include "MyTimer.h"
#include <QGridLayout>
#include <QMainWindow>
#include <QScrollArea>
#include <QSplitter>
#include <QTime>
#include <QTimer>
#include <QMouseEvent>

#include <Qthread>
#include <QDebug>
#include <QMessageBox>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2\opencv.hpp>


#include <pylon/PylonIncludes.h>

using namespace Pylon;
using namespace std;
using namespace cv;
using namespace Qt;

class Videtis : public QMainWindow
{
	Q_OBJECT

public:
	Videtis();
	QWidget *mainWindow;
	void debounce(int);

protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;

private:
	QSplitter *splitter;
	CameraThread *camThread;
	TopMenu *topMenu;
	SettingsMenu *settings;
	QVBoxLayout *box;
	QLabel *imageView;
	QScrollArea *imageViewScrollArea;
	VideoTinker *writer;
	MyTimer *timer;
	int pixmapHeight;
	int pixmapWidth;
	bool boolDraw;
	int x1;
	int y1;
	int x2;
	int y2;

signals:
	void timerStopped();
	void stopState();
	void stopped();
	void sendAOIparameterContainer(AOIparameterContainer*);
	void stopThread(int);

public slots:
	//Slots for CamerThread Signals
	void saveImage(QImage);
	void pixmapRecieved(QPixmap);
	//Slots for TopMenu Signals
	void saveImagebtnClicked();
	void startVideo(MetaDataContainer*);
	void fire();
	void draw(QMouseEvent  *);
	void resetAOI();
};