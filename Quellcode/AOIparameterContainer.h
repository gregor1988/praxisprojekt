#pragma once

#include <QObject>

class AOIparameterContainer : public QObject
{
	Q_OBJECT

public:
	AOIparameterContainer();
	~AOIparameterContainer();

	//setters
	void setOffsetXval(int64_t);
	void setOffsetYval(int64_t);
	void setWidthVal(int64_t);
	void setHeightVal(int64_t);

	//getters
	int64_t getOffsetXval();
	int64_t getOffsetYval();
	int64_t getWidthVal();
	int64_t getHeightVal();

private:
	int64_t offsetXval;
	int64_t offsetYval;
	int64_t widthVal;
	int64_t heightVal;

};
