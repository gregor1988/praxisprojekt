#include "SettingsMenu.h"


SettingsMenu::SettingsMenu()
{
	settingsLabel = new QLabel();
	settingsLabel->setText("Settings");
	QFont font;
	font.setBold(true);
	font.setPixelSize(30);
	settingsLabel->setFont(font);
	recOptions = new RecOptions();
	aoi = new AOI();
	gamma = new GammaSetting();
	settingsMenu = new QVBoxLayout();
	settingsMenu->setAlignment(AlignTop);
	exposureTimeOptions = new ExposureTime();
	menubar = new QWidget();
	//add Settings
	settingsMenu->addWidget(settingsLabel);
	settingsMenu->addWidget(recOptions);
	settingsMenu->addWidget(aoi);
	settingsMenu->addWidget(gamma);
	settingsMenu->addWidget(exposureTimeOptions);
	

	//Put it in  Srcrollarea
	menubar->setLayout(settingsMenu);
	menubar->setMaximumWidth(400);


}
void SettingsMenu::disableAllOptions() { menubar->setEnabled(false); }
void SettingsMenu::enableAllOptions(){ menubar->setEnabled(true); }

//getters
GammaSetting * SettingsMenu::getGammaSetting() { return gamma; }
AOI * SettingsMenu::getAOISetting() { return aoi; }
RecOptions * SettingsMenu::getRecOptions() { return recOptions; }
ExposureTime* SettingsMenu::getExposureTimeOptions() { return exposureTimeOptions; };

SettingsMenu::~SettingsMenu()
{
}
