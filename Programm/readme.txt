Videtis ist eine Software, die in Zukunft Aufnahme und Analyse von Bilddaten
vereinen soll.

----Installation----

Führen Sie die "Setup.exe" aus und Installieren Sie das Programm im Verzeichnis
Ihrer Wahl.

Wechseln Sie dann in das Installationverzeichnis und führen Sie die Datei
"pylon_USB_Camera_Driver.msi" aus, um die Notwendigen Treiber zu installieren.

Jetzt sollte das Programm Funktionsfähig  und eine Verknüpfung auf dem
Desktop zu sehen sein.

----Trouble shooting----

Falls die Bilder und Videos nicht gespeichert werden, könnte das daran liegen,
dass das Programm keine Schreibberechtigung hat. Führen sie zur Umgeheung
dieses Problems das Programm als Administrator aus.

Wechseln Sie dazu ins installationsverzeichniss und suchen Sie die datei
"Praxisprojekt.exe". per Rechsklick kann das Programm als Administrator
gestartet werden.

Wenn sie die Aufgenommenen Videos nicht abspielen können, fehlt Ihnen
möglicherweise der passende Kodex.

Im Installationverzeichnis finden Sie den Ordner "VideoCodecs". Darin enthalten
sind offizielle Codec-Pakete von Windows. Die Installation wird die
Standartauswahl für Videos zurück setzen. Videodateien werden dann
Standartmässig mit dem Windows Player wiedergegeben. Wenn Sie einen anderen
Player bevorzugen, können Sie diese einstellung wieder ändern.
